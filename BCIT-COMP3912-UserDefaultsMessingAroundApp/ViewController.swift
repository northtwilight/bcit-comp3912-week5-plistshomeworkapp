//
//  ViewController.swift
//  BCIT-COMP3912-UserDefaultsMessingAroundApp
//
//  Created by Massimo Savino on 2016-10-11.
//  Copyright © 2016 Massimo Savino. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var userDefaultsHeadline: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var useTouchIDLabel: UILabel!
    @IBOutlet weak var twentyFiveLabel: UILabel!
    @IBOutlet weak var yesLabel: UILabel!
    
    
    
    
    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpUserDefaults()
        
        setUpUI()
    }
    
    
    
    func setUpUserDefaults() {
        
        let defaults = UserDefaults.standard
        
        defaults.set(Constants.UserDefaults.TwentyFive, forKey: Constants.UserDefaults.KeyAge)
        defaults.set(Constants.UserDefaults.Yes, forKey: Constants.UserDefaults.UseTouchIDString)
    }
    
    
    func setUpUI() {
        
        userDefaultsHeadline.text = Constants.UserDefaults.Headline
        
        ageLabel.text = Constants.UserDefaults.KeyAge
        useTouchIDLabel.text = Constants.UserDefaults.UseTouchIDString
        twentyFiveLabel.text = String(Constants.UserDefaults.TwentyFive)
        yesLabel.text = Constants.UserDefaults.Yes
    }
}

