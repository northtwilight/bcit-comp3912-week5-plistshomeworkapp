//
//  Constants.swift
//  BCIT-COMP3912-UserDefaultsMessingAroundApp
//
//  Created by Massimo Savino on 2016-10-11.
//  Copyright © 2016 Massimo Savino. All rights reserved.
//

import Foundation

struct Constants {
    
    struct UserDefaults {
        
        static let KeyAge = "Age"
        static let TwentyFive = 25
        static let UseTouchIDString = "UseTouchID"
        static let Yes = "Yes"
        
        static let Headline = "Let me tell ya what's I gotz!"
    }
}
